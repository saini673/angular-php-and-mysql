	<?php 
		$title 				= 'User Dashboard';
		$angular_app 		= 'angular-dashboard';
		$angular_controller = 'userListing';
		
	?>
    <div class="container">
	   <?php include_once('template/header _nav_section.php'); ?>
		<div class="row">
			<div class="col-lg-12">
				<?php /* <h4 class="blog-post-title">Users List</h4> */ ?>
				<hr/>
				<div class="table-responsive">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<div class="sw-search">
								<div class="nav-search" id="nav-search">
									Filter: <span class="input-icon">
										<input placeholder="Filter User list ..." class="nav-search-input" ng-model="search"  type="text" style="width:600px;" />
										<i class="search-icon fa fa-search nav-search-icon"></i>
									</span>
								</div>
							</div>
							<div class="add_new_button"><button type="button" class="btn btn-danger fa fa-plus" ng-click="addNewUser()">Add New User</button></div>
							<div class="add_new_button"><button type="button" class="btn btn-danger fa fa-plus" ng-click="addUserFiles()">Add Files</button></div>
						</div>
						<div class="panel-body">
							<table class="table table-striped">
								<tbody ng-init="getAll()">
									<tr id="del_success_msg" style="display:none">
										<th colspan="6">
											<div class="alert alert-success fade in">
												<a href="javascript:;" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
												<strong><i class="fa fa-check"></i> Success!</strong> User Record Delete Successfully.
											</div>
										</th>
									</tr>
									<tr id="user_success_msg" style="display:none">
										<th colspan="6">
											<div class="alert alert-success fade in">
												<a href="javascript:;" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
												<strong><i class="fa fa-check"></i> Success!</strong> <span id="message_text">User Record Added Successfully</span>
											</div>
										</th>
									</tr>
									<tr><th ng-repeat="d in columns">{{d.text}}</th></tr>
									<tr id="user_id_{{d.uid}}" ng-repeat="d in users | filter:search | startFrom:(currentPage-1)*entryLimit | limitTo:entryLimit">
										<td ng-class="{'block_user': d.block_user == 1}">#{{$index +1}}</td>
										<td ng-class="{'block_user': d.block_user == 1}">{{d.first_name | capitalize }} {{d.last_name | capitalize }}</td>
										<td ng-class="{'block_user': d.block_user == 1}">{{d.email}}</td>
										<td ng-class="{'block_user': d.block_user == 1}">{{d.city}}</td>
										<td ng-class="{'block_user': d.block_user == 1}">{{d.phone}}</td>
								
										<td style="width:160px" ng-class="{'block_user': d.block_user == 1}">
											<div class="btn-group">
												<a href="javascript:;"  class	=	"btn btn-success fa fa-edit" data-toggle="tooltip" data-placement="top" title ="Edit User"  ng-click="open(d.uid);"></a>
												<a href="javascript:;"  class	=	"{{ BlockBtnClass(d.block_user) }}" id="block_btn_{{d.uid}}" data-uid = "{{ d.uid }}" data-toggle="tooltip" data-placement="top" data-original-title ="{{d.block_user == 0 && 'Block User' || 'Unblock User'}}"></a>
												<a href="javascript:;"  class	=	"btn btn-danger fa fa-trash-o"  data-toggle="tooltip" data-placement="top" title ="Delete User"  ng-click="deleteUser(d.uid)"></a>
											</div>
										</td>
									</tr>
									<tr>
										<td colspan="6" class="pagination-section">
											<div pagination="" page="currentPage" on-select-page="setPage(page)" boundary-links="true" total-items="filteredItems" items-per-page="entryLimit" class="pagination-small" previous-text="&laquo;" next-text="&raquo;"></div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php include_once('template/footer.php'); ?>