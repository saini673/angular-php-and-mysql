<?php
	ob_start();
	@session_start();
    ini_set("display_errors", "1");
	error_reporting(E_ALL);
	
	$dsn 		= 'mysql:dbname=devproject;host=localhost';
	$user 		= 'root';
	$password 	= '';

	try {
		
	    $dbh = new PDO($dsn, $user, $password);
		
		function insert($table, $fields = array(), $values = array()) {
			global $dbh;

			$fields = 	'`' . implode ( '`,`', $fields ) . '`';
			$values = 	"'" . implode ( "','", $values ) . "'";
			$sql 	= 	"INSERT INTO {$table} ($fields) VALUES($values)";
			$result	=	$dbh->prepare($sql);
			if($result){
				if ($dbh->exec($sql)) 
				{
					return $dbh->lastInsertId();;
				}
			}
			return false;
		}
		
		function update($table, $set_value = array(),$whereArr = array()) {
			global $dbh;
			
			$set_value 	 = 	implode(', ', $set_value);
			$whereArr 	 = 	implode(', ', $whereArr);
			$sql 	 	 = 	"UPDATE {$table} SET $set_value WHERE $whereArr"; 
			$result		 =	$dbh->prepare($sql);
			if($result){
				if ($dbh->exec($sql)) 
				{
					return $result;
				}
				else{
					return 'no_row_effect';
				}
			} 
			return false;
		}
		
		
		
		
		function changeblockStatus($table, $set_value = array(),$whereArr = array()) {
			global $dbh;
			
			$set_value 	 = 	implode(', ', $set_value);
			$whereArr 	 = 	implode(', ', $whereArr);
		    $sql 	 	 = 	"UPDATE {$table} SET $set_value WHERE $whereArr"; 
			$result		 =	$dbh->prepare($sql);
			if($result){
				if ($dbh->exec($sql)) 
				{
					return $result;
				}
				else{
					return 'no_row_effect';
				}
			} 
			return false;
		}
		
		function deleteUser($table,$id=null) {
			global $dbh;
			
			$sql 	     = 	"DELETE FROM {$table} WHERE uid	=	$id";
			$result		 =	$dbh->prepare($sql);
			if($result){
				if ($dbh->exec($sql)) 
				{
					return $result;
				}
			} 
			return false;
		}
		
		function CheckEmail($table,$email=null){
			global $dbh;

			$query = $dbh->prepare( "SELECT `email` FROM {$table} WHERE `email` = ?" );
			$query->bindValue(1, $email);
			$query->execute();
			return $query->rowCount();
		}
		
		function UserLogin($table,$email,$password){
			global $dbh;
			$query = $dbh->prepare( "SELECT * FROM {$table} WHERE `email` = '".$email."' AND `password` = '".md5($password)."'" );
			$query->execute();
			return $query->rowCount();
			
		}
		function getUserDetail($table,$email,$password){
			global $dbh;
			$query = $dbh->prepare( "SELECT * FROM {$table} WHERE `email` = '".$email."' AND `password` = '".md5($password)."'" );
			$query->execute();
			return $query->fetchAll(PDO::FETCH_OBJ);
		}
		function getUserDetailByid($table,$uid){
			global $dbh;
			$query = $dbh->prepare( "SELECT * FROM {$table} WHERE `uid` = '".$uid."'" );
			$query->execute();
			$result 	=	$query->fetchAll(PDO::FETCH_OBJ);
			if($query->rowCount() >0){
				return $result[0];
			}
			else{
				return 'no_record';
			}
		}
		function getAllUser($table){
			global $dbh;
			$query = $dbh->prepare("SELECT * FROM {$table} order by email,first_name,last_name");
			$query->execute();
			$results 	=	$query->fetchAll(PDO::FETCH_ASSOC);
			return $results; 
		}
		function getuserByUid($table,$uid){
			global $dbh;
			$qry = "SELECT uid,first_name,last_name,city,country,address,phone FROM {$table} WHERE `uid` = '".$uid."'";
			$query = $dbh->prepare($qry);
			$query->execute();
			$result 	=	$query->fetchAll(PDO::FETCH_OBJ);
			if($query->rowCount() >0){
				return $result[0];
			}
			else{
				return false;
			}
		}
		
		function existProfilePicById($table,$uid){
			global $dbh;
			$qry = "SELECT * FROM {$table} WHERE `id` = '".$uid."'";
			$query = $dbh->prepare($qry);
			$query->execute();
			if($query->rowCount() == 1){
				return 'exist';
			}else{
				return 'pass';
			}
			
		}
		
		function ProfilePicById($table,$uid){
			global $dbh;
			$qry = "SELECT * FROM {$table} WHERE `id` = '".$uid."'";
			$query = $dbh->prepare($qry);
			$query->execute();
			$result 	=	$query->fetchAll(PDO::FETCH_OBJ);
			if($query->rowCount() >0){
				return $result[0];
			}
			else{
				return false;
			}
		}
		
		function MoveCorpedImage($Image,$uid,$filename){
			list($type, $Image) 	= explode(';', $Image);
			list(, $Image)      	= explode(',', $Image);
			$Image 					= base64_decode($Image);
			$path = 'uploads/'.$uid.'_'.$filename.".jpg";
			file_put_contents($path, $Image);
			$Image = $path;
			return $Image;
		}
		
		function UploadsUserFileManger($fileNameArr,$fileTempNameArr,$userId,$target_dir){
			
			if(sizeof($fileNameArr) > 0){
				
				$galleryDir = "uploads/gallery/";
				if(!is_dir($galleryDir)) {
					mkdir($galleryDir,0777);
					$targetDir	=	$galleryDir.$target_dir."/";
					if(is_dir($galleryDir) && !is_dir($targetDir)) {
						mkdir($targetDir,0777);
					}
				}
				$targetDir	=	$galleryDir.$target_dir."/";
				$count	=	count($fileNameArr);
				for($i=0;$i<=$count; $i++){
					move_uploaded_file($fileTempNameArr[$i],$targetDir.time().'_'.$fileNameArr[$i]);
				}
			}
			
		}
		
	} 
	catch (PDOException $e) {
		echo 'Connection failed: ' . $e->getMessage();
	}

	
	function base_url(){
		return sprintf(
			"%s://%s%s",
			isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
			$_SERVER['SERVER_NAME'],
			$_SERVER['REQUEST_URI']
		  );
	}
	
	function current_pagename(){
		return basename($_SERVER['PHP_SELF'], ".php");
	}


?>