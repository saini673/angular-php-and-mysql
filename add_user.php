<?php
	require_once('functions/function.php');
	global $dbh;
	$post_data 		= json_decode(file_get_contents("php://input"));
	$column_field 	= array();
	$value_field 	= array();
	$email_field 	= array();
	
	// Add user section //
	foreach($post_data as $key=>$data){
		$column_field[]  	 =  $key;
		if($key == 'password'){
		   $data   			= md5($data);
		}
		if($key == 'email'){
		   $email_field[]   =  $data;
		}
		$value_field[]  	=  $data;
	}
	
	if(sizeof($post_data) > 0){
		
		$check_email	=	CheckEmail('users',$email_field[0]);
		if($check_email > 0){
			$message = array('message' => "", 'error' => 'User Already exists with same email');
			$reponse = json_encode($message);
			print_r($reponse);
		}
		else{
			$result 			= 	insert('users', $column_field, $value_field);
			if ($result) {
				$message = array('message' => "User Created Successfully!!!", 'error' => '');
				$reponse = json_encode($message);
				print_r($reponse);
			} else {
				$message = array('message' => "", 'error' => 'Error In inserting record');
				$reponse = json_encode($message);
				print_r($reponse);
			}
		}
		
	}
?>

