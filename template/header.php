<?php 

	ob_start();
	session_start();
	ini_set("display_errors", "1");
	error_reporting(E_ALL);
	require_once('functions/function.php'); 
	
?>
<!DOCTYPE html>
<html ng-app="<?php echo $angular_app; ?>">
<head>
    <title><?php echo $title; ?></title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/custom.css" type="text/css" />
	<link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" />
	<link rel="stylesheet" href="css/dropzone.css" type="text/css" />
	<script type="text/javascript">var base_url = 'http://localhost/post_angular/';</script>
</head>
<body ng-controller="<?php echo $angular_controller; ?>">