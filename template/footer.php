	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>  
	<script type="text/javascript" src="js/angular.min.js"></script>   
    <script type="text/javascript" src="js/angular-validator.js"></script>
	<script type="text/javascript" src="js/angular-messages.js"></script>
	<script type="text/javascript" src="js/app.js"></script>
	<?php if(isset($_SESSION['UserID']) && !empty($_SESSION['UserID'])) { ?>
		
		<script type="text/javascript" src="js/angular-img-cropper.min.js"></script>
		<script type="text/javascript" src="js/ui-bootstrap-tpls-0.9.0.js"></script>
		<<script type="text/javascript" src="js/dropzone.min.js"></script>
		
		<script type="text/javascript" src="js/angular-dropzone.js"></script>
		<script type="text/javascript" src="js/userlist_app.js"></script>
		
	<?php } ?>
	
</body>
</html>