	<?php 
	
		include_once('template/header.php'); 
		if(!isset($_SESSION['UserID']) && empty($_SESSION['UserID']))
		{
			header('location:login.php');
			exit();
		}
		else
		{
			$userDetail		=	getUserDetailByid('users',$_SESSION['UserID']);
			if($userDetail == 'no_record'){
				header('location:logout.php');
				exit();
			}
			$userpicture	=	ProfilePicById('user_profile',$_SESSION['UserID']);
			
		}  
	?>
	<div class="row">
		<div class="col-lg-12">
			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="javascript:;">Leave Application</a>
					</div>
					
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav">
							<li class="active"><a href="javascript:;">Users <span class="sr-only">(current)</span></a></li>
							<li><a href="#">Leave Applications</a></li>
						</ul>
						<div class="btn-group user-detail pull-right">
							<a class="btn btn-danger" href="javascript:;"><i class="fa fa-user fa-fw"></i> <?php echo ucfirst($userDetail->first_name).' '.ucfirst($userDetail->last_name); ?></a>
							<a class="btn btn-danger dropdown-toggle" data-toggle="dropdown" href="javascript:;">
							<span class="fa fa-caret-down"></span></a>
							<ul class="dropdown-menu">
								<li><a href="javascript:;" ng-click="open('<?php echo $userDetail->uid; ?>');"><i class="fa fa-pencil fa-fw"></i> Edit</a></li>
								<li><a href="javascript:;" ng-click="changePassModal('<?php echo $userDetail->uid; ?>');"><i class="fa fa-key fa-fw"></i> Change Password</a></li>
								
								<li class="divider"></li>
								<li><a href="logout.php"><i class="fa fa-power-off fa-fw"></i> Logout</a></li>
								<li><a href="javascript:;"><i class="fa fa-trash-o fa-fw"></i> Delete Account</a></li>
							</ul>
						</div>
					</div>
				</div>
			</nav>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<h1 class="heading-section text-center">Welcome to&nbsp;<?php echo ucfirst($userDetail->first_name).' '.ucfirst($userDetail->last_name); ?> </h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			<div class="user_picture">
				<img class="user_img" src="<?php if(!empty($userpicture->path)) { echo $userpicture->path; } else { ?>images/default.png <?php } ?>" />
			</div>
			<div class="change_pro_btn">
				<button class="btn btn-danger" data-toggle="tooltip" data-placement="top" data-original-title ="Change Picture"  ng-click="ChangePicture(<?php echo $userDetail->uid; ?>)" >Change Picture</button>
			</div>
		</div>
	</div>
	