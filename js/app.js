angular.module('angular-validator-demo', ['angularValidator']);


angular.module('angular-validator-demo').controller('DemoCtrl', function($window,$scope,$http,$timeout) {
	$scope.sucess_msg 	= false;
	$scope.errors 		= [];
    $scope.msgs 		= [];
	
	// User Register submit form //
	
	$scope.submitMyForm = function() {
			$scope.errors.splice(0, $scope.errors.length); // remove all error messages
            $scope.msgs.splice(0, $scope.msgs.length);
			var first_name   	=  $scope.form.first_name;
			var last_name   	=  $scope.form.last_name;
			var email   		=  $scope.form.email;
			var city   			=  $scope.form.city;
			var country   		=  $scope.form.country;
			var address   		=  $scope.form.address;
			var phone   		=  $scope.form.phone;
			var password   		=  $scope.form.confirmPassword;
			$http.post('add_user.php', {'first_name': first_name, 'last_name': last_name, 'email': email,'city': city, 'country': country,'address':address,'phone':phone,'password':password}
			).success(function(data, status, headers, config) {
				   if (data.message != '')
					{
					   $scope.sucess_msg = true;
					   $scope.myForm.reset();
					  
						var ErrorClass = angular.element( document.querySelector( '#email_id' ) );
						ErrorClass.removeClass('input-email-error');
						
						$scope.msgs.push(data.message);

						$scope.callAtTimeout = function() {
							$window.location.href = base_url+'login.php'; 
						}

						$timeout( function(){ $scope.callAtTimeout(); }, 5000);
						
					}
					else
					{
						if( data.error== 'User Already exists with same email')
						{
							var ErrorClass = angular.element( document.querySelector( '#email_id' ) );
							ErrorClass.addClass('input-email-error'); 
						}
						else
						{
							var ErrorClass = angular.element( document.querySelector( '#email_id' ) );
							ErrorClass.removeClass('input-email-error');
						}
						 $scope.sucess_msg = false;
						$scope.errors.push(data.error);
					}
			}).error(function(data, status) { 
				$scope.errors.push(status);
			});
	};

	
	 // user login //
	 
		$scope.Userlogin = function() {
			$scope.errors.splice(0, $scope.errors.length); // remove all error messages
            $scope.msgs.splice(0, $scope.msgs.length);
		
			var email   		=  $scope.form.user_email;
			var password   		=  $scope.form.user_password;
			$http.post('user_login.php', {'email': email,'password':password}
		).success(function(data, status, headers, config) {
			 if(data.login_message!='' && data.login_message == 1){
				$window.location.href = base_url+'dashboard.php';
			 }else {
				$scope.error_msg = true;
				$scope.errors.push(data.error);
			 }
		}).error(function(data, status) { 
			$scope.error_msg = true;
			$scope.errors.push(data.error);
		});
	};
	$scope.myCustomValidator = function(text) {
		return true;
	};


	$scope.anotherCustomValidator = function(text) {
		if (text === "rainbow") {
			return true;
		} else return "type in 'rainbow'";
	};


	$scope.passwordValidator = function(password) {

		if (!password) {
			return;
		}
		else if (password.length < 6) {
			return "Password must be at least " + 6 + " characters long";
		}
		else if (!password.match(/[A-Z]/)) {
			return "Password must have at least one capital letter";
		}
		else if (!password.match(/[0-9]/)) {
			return "Password must have at least one number";
		}

		return true;
	};
}).factory('customMessage', function () {
    // invalid message service with message function
    return {
        // scopeElementModel is the object in scope version, element is the object in DOM version
        message: function (scopeElementModel, element) {
            var errors = scopeElementModel.$error;
            if (errors.maxlength) {
                // be careful with the quote
                return "'Should be no longer than " + element.attributes['ng-maxlength'].value + " characters!'";
            } else {
                // default message
                return "'This field is invalid!'";
            }
        }
    };
});