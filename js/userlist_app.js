		var userApp = angular.module('angular-dashboard', ['angular-img-cropper','ngDropzone','UserValidation','ngMessages','ui.bootstrap']);
	
		userApp.run(function($rootScope) {
			
			$rootScope.BlockBtnClass = function(blockstatus) {
				if(blockstatus	==	0)
				{
					return "btn btn-warning fa fa-ban block_button";
				}
				else if(blockstatus	== 1)
				{
					return "btn btn-primary fa fa-ban unblock_button";
				}
			};
			
		});
	
		// Custom password and confirm password match module //
	
			angular.module('UserValidation', []).directive('validPasswordC', function () {
				return {
						require: 'ngModel',
						link: function (scope, elm, attrs, ctrl) {
							
							ctrl.$parsers.unshift(function (viewValue, $scope) {
								
								if(attrs['name'] == 'C_pass'){
									var noMatch = viewValue != scope.changepwdform.password.$viewValue
								}else{
									var noMatch = viewValue != scope.adduserform.password.$viewValue
								}
								
								ctrl.$setValidity('noMatch', !noMatch)
							})
						}
					}
			});
	
		// End custom password and confirm password match module //
	
		// Custom bootstrap angular modal directive validation //
	
			var ValidSubmit = ['$parse', function ($parse) {
				return {
					compile: function compile(tElement, tAttrs, transclude) {
						return {
							post: function postLink(scope, element, iAttrs, controller) {
								var form = element.controller('form');
								form.$submitted = false;
								var fn = $parse(iAttrs.validSubmit);
								element.on('submit', function(event) {
									scope.$apply(function() {
										element.addClass('ng-submitted');
										form.$submitted = true;
										if(form.$valid) {
											fn(scope, {$event:event});
										}
									});
								});
								scope.$watch(function() { return form.$valid}, function(isValid) {
									if(form.$submitted == false) return;
									if(isValid) {
										element.removeClass('has-error').addClass('has-success');
									} else {
										element.removeClass('has-success');
										element.addClass('has-error');
									}
								});
							}
						}
					}
				}
			}]
		
			userApp.directive('validSubmit', ValidSubmit);
		
	// End custom bootstrap angular modal directive validation //
		
	// On click anchor tag change block user status //
	
		userApp.directive('a', function($modal) {
			return {
				restrict: 'E',
				link: function(scope, elem, attrs) {
					
				//  user status block or Unblock by user-id  //
				
					 if(attrs.class == 'btn btn-warning fa fa-ban block_button'){
						elem.on('click', function(e){
							
							var uid = $(this).attr("data-uid");
							scope.blockuser  = uid;
							var modalInstance = $modal.open({
								backdrop: 'static',
								animation: true,
								keyboard :false,
								backdropClick:false,
								templateUrl: 'views/BlockuserModal.html',
								controller: 'BlockModalController',
								size: 'sm',
								resolve: {
									blockuser: function () {
										return scope.blockuser;
									}
								}
							});
							
							
						});
					}
					else if(attrs.class == 'btn btn-primary fa fa-ban unblock_button'){
						elem.on('click', function(e){
								var uid = $(this).attr("data-uid");
								
								scope.unblockuser = uid;
								var modalInstance = $modal.open({
								backdrop: 'static',
								animation: true,
								keyboard :false,
								backdropClick:false,
								templateUrl: 'views/UnBlockuserModal.html',
								controller: 'UnBlockModalController',
								size: 'sm',
								resolve: {
									unblockuser: function () {
										return scope.unblockuser;
									}
								}
							});
							
						});
					}				
				}
		   };
		});
		
		// End on click anchor tag change block user status //
		
		// Pagination in angularjs filter for user dashboard //
	
			userApp.filter('startFrom', function() {
				return function(input, start) {
					if(input) {
						start = +start; //parse to int
						return input.slice(start);
					}
					return [];
				}
			});
	
		// End pagination in angularjs filter //
		
		
		// Character capitalize in angularjs filter //
	
			userApp.filter('capitalize', function() {
				return function(input) {
				  return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
				}
			});
    
		// End character capitalize in angularjs filter //
	
	    // User listing controller //
		
		angular.module('angular-dashboard').controller('userListing', function($modal,$scope,$http) 
		{
			
			$scope.getAll = function(){
				$http.get("userlist.php").success(function(response){
					$scope.users 			= response.records;
					$scope.currentPage 		= 1; //current page
					$scope.entryLimit 		= 10; //max no of items to display in a page
					$scope.filteredItems 	= $scope.users.length; //Initially for no filter  
					$scope.totalItems 		= $scope.users.length;
				});
				$scope.setPage = function(pageNo) {
					$scope.currentPage = pageNo;
				};
				
			}
		
			// Start user List column name set //
			
			$scope.columns = [
								{text:"S.NO.",predicate:"s.no.",sortable:true,dataType:"number"},
								{text:"Name",predicate:"first_name",sortable:true},
								{text:"Email",predicate:"email",sortable:true},
								{text:"City",predicate:"city",sortable:true},
								{text:"Phone",predicate:"phone",sortable:true},
								{text:"Action",predicate:"",sortable:false}
							];
						
			// End user List column name set //
			
			
			// Bootstrap modal open //
			
			$scope.open = function (uid) 
			{
				$http.get('userlist.php/?uid='+uid).success(function(data) 
				{
					$scope.user = data.userdetail;
					var modalInstance = $modal.open({
						backdrop: 'static',
						animation: true,
						keyboard :false,
						backdropClick:false,
						templateUrl: 'views/edituserModal.html',
						controller: 'editModalController',
						size: 'lg',
						resolve: {
							user: function () {
								return $scope.user;
							}
						}
					});
				});
			};	
			
			// Change Picture Modal//
			
			$scope.ChangePicture = function (uid) 
			{
				$scope.changepicture = uid;
				angular.element( document.querySelector( '#loading_img' ) ).hide();
				angular.element( document.querySelector( '.cropedImg' ) ).hide();
				angular.element( document.querySelector( '.cropimageArea' ) ).show();
				angular.element( document.querySelector( '.modal-footer' ) ).show();
				var modalInstance = $modal.open({
					backdrop: 'static',
					animation: true,
					keyboard :false,
					backdropClick:false,
					windowClass :'change-picture-section',
					templateUrl: 'views/changePictureModal.html',
					controller: 'ChangePictureModalController',
					size: 'lg',
					resolve: {
						changepicture: function () {
							return $scope.changepicture;
						}
					}
				});
			};	
			
			// Change Picture Modal//
			
			// Change user password Modal //
			
				$scope.changePassModal = function(uid){
					$scope.changepassword = uid;
					var modalInstance = $modal.open({
						backdrop: 'static',
						animation: true,
						keyboard :false,
						backdropClick:false,
						templateUrl: 'views/changepasswordModal.html',
						controller: 'ChangePasswordModalController',
						size: 'lg',
						resolve: {
							changepassword: function () {
								return $scope.changepassword;
							}
						}
					});
				};
			
			// Change user password Modal //
			
			// add new user bootstrap modal //
			
			$scope.addNewUser = function () 
			{
				var modalInstance = $modal.open({
					backdrop: 'static',
					animation: true,
					keyboard :false,
					backdropClick:false,
					templateUrl: 'views/adduserModal.html',
					controller: 'adduserModalController',
					size: 'lg'
				});
			};	
			
			// End Bootstrap modal open //
			
			/* 
				Open user add files in Bootstrap modal
			*/
			
			$scope.addUserFiles = function () 
			{
				var modalInstance = $modal.open({
					backdrop: 'static',
					animation: true,
					keyboard :false,
					backdropClick:false,
					templateUrl: 'views/adduserFilesModal.html',
					controller: 'adduserFilesModalController',
					size: 'lg'
				});
			};	
			
			/* 
				End user add files in Bootstrap modal
			*/
			
			
			$scope.deleteUser = function (uid) 
			{
				$scope.deleteuser = uid;
				var modalInstance = $modal.open({
					backdrop: 'static',
					animation: true,
					keyboard :false,
					backdropClick:false,
					templateUrl: 'views/DeleteuserModal.html',
					controller: 'DeleteModalController',
					size: 'sm',
					resolve: {
						deleteuser: function () {
							return $scope.deleteuser;
						}
					}
				});	
			};
					
		}).directive('toggle', function(){
			  return {
				restrict: 'A',
				link: function(scope, element, attrs){
					if (attrs.toggle=="tooltip"){
						$(element).tooltip();
					}
					if (attrs.toggle=="popover"){
						$(element).popover();
					}
				}
			  };
			});
	
	//  End User listing controller //
		
		
	/*
		Change user profile picture controller 
	*/
		userApp.controller('ChangePictureModalController',function($window,$scope,$modal,$modalInstance, changepicture,$http,$timeout) {
			
			$scope.changepicture 					= changepicture;
			$scope.cropper 							= {};
			$scope.cropper.sourceImage 				= null;
			$scope.cropper.croppedImage   			= null;
			$scope.bounds 							= {};
			$scope.bounds.left 						= 0;
			$scope.bounds.right						= 0;
			$scope.bounds.top 						= 0;
			$scope.bounds.bottom 					= 0;
			$scope.cancel 							= function (){
				$modalInstance.close();
			};
			
			$scope.SaveProfilePicture				= function(srcimage,uid){
				
				var fileLength	=	angular.element( document.querySelector( '#inputfilename' ) )[0].files['length'];
				if(fileLength == 0){
					angular.element( document.querySelector( '#error_message' ) ).show();
					var message	=	'Please select your picture.';
					angular.element( document.querySelector( '#error_message' ) ).html('<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a><strong><i class="fa fa-check"></i> Error! </strong> <span id="error_text_msg">'+message+'</span></div>');
					
					return false;
				}
				else
				{
					var filename = angular.element( document.querySelector( '#inputfilename' ) )[0].files[0]['name'];
					var filetype = angular.element( document.querySelector( '#inputfilename' ) )[0].files[0]['type'];
					
					if(filetype != 'image/jpeg'){
						angular.element( document.querySelector( '#error_message' ) ).show();
						var message	=	'Please select only jpg,jpeg,png valid extension.';
						angular.element( document.querySelector( '#error_message' ) ).html('<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a><strong><i class="fa fa-check"></i> Error! </strong> <span id="error_text_msg">'+message+'</span></div>');
						
						return false;
					}
					else
					{
						angular.element( document.querySelector( '#error_message' ) ).hide();
						angular.element( document.querySelector( '#error_text_msg' ) ).html('');
						
						angular.element( document.querySelector( '#loading_img' ) ).show();
						angular.element( document.querySelector( '.cropedImg' ) ).show();
						angular.element( document.querySelector( '.cropimageArea' ) ).hide();
						angular.element( document.querySelector( '.modal-footer' ) ).hide();
						
						if(srcimage!='' && srcimage!='null'){
						
							$http.post('uploadfile.php', {'uid': uid,'imagepath': srcimage,'filename':filename}
							 ).success(function(data, status, headers, config) {
									
								
								if(data.message!=''){
									angular.element( document.querySelector( '#loading_img' ) ).hide();
									angular.element( document.querySelector( '.cropedImg' ) ).hide();
									angular.element( document.querySelector( '#uploadfilemsg' ) ).show();
									$scope.callAtTimeout = function() {
										angular.element( document.querySelector( '.user_img' ) ).attr("src",data.message);
										angular.element( document.querySelector( '#uploadfilemsg' ) ).hide();
										$modalInstance.close();
									}
									$timeout( function(){ $scope.callAtTimeout(); }, 4000); 
								}
								
							}).error(function(data, status) { 
								$scope.errors.push(status);
							});
						} 
					}
					
				}
				
			};
			
		});
		
	
		/*
			End change user profile picture controller 
		*/  
		
		
		/*
			Change user password controller 
		*/
		
		userApp.controller('ChangePasswordModalController',function($window,$scope,$modal,$modalInstance,changepassword,$http,$timeout) {
			
			$scope.changepassword 					= changepassword;
			$scope.cancel 							= function (){
				$modalInstance.close();
			};
			
			$scope.ChangePass			=	function (uid){
				var old_pass 			= 	angular.element( document.querySelector( '#old_pass' ) ).val();
				console.log(old_pass);
			};
			
		});
	
		 /*
			End change user password controller 
		*/
		
	
		/*
			Open user edit bootstrap modal 
		*/
	
		userApp.controller('editModalController',function($window,$scope, $modal,$modalInstance, user,$http,$timeout) {
			
			$scope.user 					= user;
			$scope.edit_first_name 			= user.first_name;
			$scope.edit_last_name 			= user.last_name;
			$scope.edit_city 				= user.city;
			$scope.edit_country 			= user.country;
			$scope.edit_address 			= user.address;
			$scope.edit_phone 				= user.phone;
			$scope.cancel 					= function (){
				$modalInstance.close();
			};
			
			$scope.update_msg 	= false;
			$scope.errors 		= [];
			$scope.msgs 		= [];
			var main 			= this;
			$scope.updateuser 	= function(user) {
				$scope.errors.splice(0, $scope.errors.length); 
				$scope.msgs.splice(0, $scope.msgs.length);
				
				var isValid 	= 	angular.element( document.querySelector( '#edit_user' ) ).find('.ng-invalid');
				
				if (isValid.length == 0){ 
				
					var first_name 		= angular.element( document.querySelector( '#first_name' ) ).val();
					var last_name  		= angular.element( document.querySelector( '#last_name' ) ).val();
					var city 			= angular.element( document.querySelector( '#city' ) ).val();
					var country 		= angular.element( document.querySelector( '#country' ) ).val();
					var address 		= angular.element( document.querySelector( '#address' ) ).val();
					var phone 			= angular.element( document.querySelector( '#phone' ) ).val();
						
					$http.post('edit_user.php', {'uid': user.uid,'first_name': first_name, 'last_name': last_name,'city': city, 'country': country,'address':address,'phone':phone}
					 ).success(function(data, status, headers, config) {
		
							if (data.message != ''){
								$modalInstance.close();
								$scope.usermessage = data.message;
								var modalInstance = $modal.open({
									backdrop: 'static',
									animation: true,
									keyboard :false,
									backdropClick:false,
									templateUrl: 'views/SuccessupdateModal.html',
									controller: 'SuccessupdateController',
									size: 'sm',
									resolve: {
										usermessage: function () {
											return $scope.usermessage;
										}
									}
								});
							}
						}).error(function(data, status) { 
							$scope.errors.push(status);
						});
				}  
			};
		});
	
		
		/*
			End open edit bootstrap modal 
		*/
		
		/*
			Start user success update bootstrap modal
		*/
		
	
			userApp.controller('SuccessupdateController',function($window,$scope,$modalInstance,usermessage,$http,$timeout) {
				$scope.usermessage = usermessage;
				$scope.callAtTimeout = function() {
					$window.location.href = base_url+'dashboard.php'; 
				}
				$timeout( function(){ $scope.callAtTimeout(); }, 4000);
			});
	
		/*
			End user success bootstrap modal
		*/
		
		
		/*
			Start user delete bootstrap modal
		*/
		
	
			userApp.controller('DeleteModalController',function($window,$scope,$modalInstance,deleteuser,$http,$timeout) {
				$scope.deleteuser = deleteuser;
				$scope.cancel = function () {
					$modalInstance.close();
				};
				
				$scope.deleteuserbyID = function (deleteuser){
					$http.post('delete_user.php', {'uid': deleteuser}
					).success(function(data, status, headers, config) {  
						
						if(data.message!='' && data.message == 1){
							angular.element( document.querySelector( '#user_id_'+deleteuser ) ).remove();
							$modalInstance.close();
							angular.element( document.querySelector( '#del_success_msg' ) ).show();
							$scope.callAtTimeout = function() {
								angular.element( document.querySelector( '#del_success_msg' ) ).hide();
							}
							$timeout( function(){ $scope.callAtTimeout(); }, 4000); 
						}
					}).error(function(data, status) { 
						$scope.errors.push(status);
					});
				};
				
				
			});
	
		/*
			End user delete bootstrap modal
		*/
		 
		
		/*
			Add new user bootstrap modal controller
		*/
		
		
			userApp.controller('adduserModalController',function($window,$scope, $modal,$modalInstance,$http,$timeout) {
				$scope.addUser = function() {
						
						var first_name 		= angular.element( document.querySelector( '#first_name' ) ).val();
						var last_name  		= angular.element( document.querySelector( '#last_name' ) ).val();
						var email  			= angular.element( document.querySelector( '#email' ) ).val();
						var city 			= angular.element( document.querySelector( '#city' ) ).val();
						var country 		= angular.element( document.querySelector( '#country' ) ).val();
						var address 		= angular.element( document.querySelector( '#address' ) ).val();
						var phone 			= angular.element( document.querySelector( '#phone' ) ).val();
						var password 		= angular.element( document.querySelector( '#confirmPassword' ) ).val();
						$http.post('add_user.php', {'first_name': first_name, 'last_name': last_name, 'email': email,'city': city, 'country': country,'address':address,'phone':phone,'password':password}
						).success(function(data, status, headers, config) {
							if (data.message != '')
							{
								var ErrorClass = angular.element( document.querySelector( '#email' ) );
								ErrorClass.removeClass('input-email-error');
								$modalInstance.close();
								angular.element( document.querySelector( '#user_success_msg' ) ).show();
								$scope.callAtTimeout = function() {
									angular.element( document.querySelector( '#user_success_msg' ) ).hide();
									$window.location.reload();
								}
								$timeout( function(){ $scope.callAtTimeout(); }, 4000); 
							}
							else
							{
								if( data.error== 'User Already exists with same email')
								{
									var ErrorClass = angular.element( document.querySelector( '#email' ) );
									ErrorClass.addClass('input-email-error'); 
									angular.element( document.querySelector( '#email-already-exit' ) ).show();
								}
								else
								{
									var ErrorClass = angular.element( document.querySelector( '#email' ) );
									ErrorClass.removeClass('input-email-error');
									angular.element( document.querySelector( '#email-already-exit' ) ).hide();
								}
								console.log(data.error);
							}
					}).error(function(data, status) { 
						$scope.errors.push(status);
					});
						
				};
				
				$scope.addFormReset = function () {
					$scope.adduserform = {};
				};
				$scope.cancel = function () {
					$modalInstance.close();
				};
			});
	
		/*
			End add new user bootstrap modal
		*/
		
		/*
			User block bootstrap modal
		*/
		
			userApp.controller('BlockModalController', function($window,$scope,$modalInstance,blockuser,$http,$timeout) {
				$scope.blockuser = blockuser;
				
				$scope.cancel = function () {
					$modalInstance.close();
				};
				
				$scope.blockUserbyID = function (uid){
				
					$http.post('block_user.php', {'uid': uid,'block_user':1}
					).success(function(data, status, headers, config) {  
						if(data!='' && data.message==1){
							
							$modalInstance.close();
							
							angular.element( document.querySelector( '#user_success_msg' ) ).show();
							angular.element( document.querySelector( '#user_id_'+uid) ).find('td').addClass('block_user');
							angular.element( document.querySelector( '#message_text' ) ).html('User block Successfully.');
							
							angular.element( document.querySelector( '#block_btn_'+uid) ).attr('class','btn btn-primary fa fa-ban unblock_button');
							angular.element( document.querySelector( '#block_btn_'+uid) ).attr('data-original-title','Unblock User');

							$scope.callAtTimeout = function() {
								angular.element( document.querySelector( '#user_success_msg' ) ).hide();
								angular.element( document.querySelector( '#message_text' ) ).html(' ');
							}
							$scope.StatusATTimeout = function() {
								angular.element( document.querySelector( '#user_success_msg' ) ).hide();
								angular.element( document.querySelector( '#message_text' ) ).html(' ');
								$window.location.reload();
							}
							$timeout( function(){ $scope.callAtTimeout(); }, 4000); 
							$timeout( function(){ $scope.StatusATTimeout(); }, 4000); 
							
						}
						
					}).error(function(data, status) { 
						$scope.errors.push(status);
					});
				};
			});
			
		/*
			End user block bootstrap modal
		*/
		
		
		/*
			Unblock user bootstrap modal
		*/
	   
			userApp.controller('UnBlockModalController',function($window,$scope,$modalInstance,unblockuser,$http,$timeout) {
				$scope.unblockuser = unblockuser;
				
				$scope.cancel = function () {
					$modalInstance.close();
				};
				
				$scope.UnblockUserbyID = function (uid){
					
					$http.post('block_user.php', {'uid': uid,'block_user':0}
					).success(function(data, status, headers, config) {  
						
						if(data!='' && data.message==1){
							$modalInstance.close();
							angular.element( document.querySelector( '#user_success_msg' ) ).show();
							angular.element( document.querySelector( '#user_id_'+uid) ).find('td').removeClass('block_user');
							angular.element( document.querySelector( '#message_text' ) ).html('User Unblock Successfully.');
							angular.element( document.querySelector( '#block_btn_'+uid) ).attr('class','btn btn-warning fa fa-ban block_button');
							angular.element( document.querySelector( '#block_btn_'+uid) ).attr('data-original-title','Block User');
							$scope.callAtTimeout = function() {
								angular.element( document.querySelector( '#user_success_msg' ) ).hide();
								angular.element( document.querySelector( '#message_text' ) ).html(' ');
								$scope.StatusATTimeout();
							}
							$scope.StatusATTimeout = function() {
								angular.element( document.querySelector( '#user_success_msg' ) ).hide();
								angular.element( document.querySelector( '#message_text' ) ).html(' ');
								$window.location.reload();
							}
							$timeout( function(){ $scope.callAtTimeout(); }, 4000); 
							$timeout( function(){ $scope.StatusATTimeout(); }, 4000); 
						}
					}).error(function(data, status) { 
						$scope.errors.push(status);
					});
				};
			});
			
		/*
			End unblock user bootstrap modal
		*/
		
		
		/*  
			Add user Files Dropzone Controller
		*/
		
		userApp.controller('adduserFilesModalController',function($window,$scope, $modal,$modalInstance,$http,$timeout,$log) {
			
			
			
			$scope.dropzoneConfig = {
				url:'uploadfile.php',
				autoProcessQueue: false,
				uploadMultiple: true,
				parallelUploads: 100,
				maxFiles: 100,
				acceptedFiles: "image/*",

				init: function () {

					var submitButton 	= document.querySelector("#submit-all");
					var wrapperThis 	= this;

					submitButton.addEventListener("click", function () {
						wrapperThis.processQueue();
					});

					this.on("addedfile", function (file) {

                    // Create the remove button
                    var removeButton = Dropzone.createElement("<button class='btn btn-lg dark'>Remove File</button>");

                    // Listen to the click event
                    removeButton.addEventListener("click", function (e) {
                        // Make sure the button click doesn't submit the form:
                        e.preventDefault();
                        e.stopPropagation();

                        // Remove the file preview.
                        wrapperThis.removeFile(file);
                        // If you want to the delete the file on the server as well,
                        // you can do the AJAX request here.
                    });

                    // Add the button to the file preview element.
                    file.previewElement.appendChild(removeButton);
                });

               /*  this.on('sendingmultiple', function (data, xhr, formData) {
                    formData.append("Username", $("#Username").val());
                }); */
            }
			};
			  
			$scope.cancel = function () {
				$modalInstance.close();
			};
		});