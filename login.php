<?php 
	$title 				= 'User Login';
	$angular_app 		= 'angular-validator-demo';
	$angular_controller = 'DemoCtrl';
	include_once('template/header.php'); 
	

?>
    <div class="container">
		
		<div class="row">
			<div class="col-lg-12">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<form angular-validator-submit="Userlogin()" name="userloginform" class="form-horizontal" novalidate angular-validator>
						<h4 class="text-center heading-section">User login</h4>
							<div ng-show="error_msg" id="login_error_msg">
								<div class = "alert alert-danger alert-dismissable" ng-show="error_msg">
									<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">&times;</button>
									<strong>Error!</strong> <span class="text-center" ng-repeat="error in errors">{{ error }} </span>
								</div>
							</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Email</label>
							<div class="col-sm-8">
								<input  type = "email"
									name = "user_email"
									validate-on="email"
									class = "form-control"
									id  = "user_email_id"
									ng-model = "form.user_email"
									invalid-message="'Enter valid email address'"
									required-message="'Please enter email address.'"
									required />
							</div>	
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Password</label>
							<div class="col-sm-8">
								<input  type = "password"
										name = "user_password"
										class = "form-control"
										ng-model = "form.user_password"
										validator = "passwordValidator(form.user_password) === true"
										invalid-message = "passwordValidator(form.user_password)"
										validate-on="dirty"
										required></div>
						</div>
						
						<div class="form-group pull-right loginsubmit">
							<div class="col-sm-offset-2 col-sm-10">
								<button type="submit" class="btn btn-primary">Login</button>
								<button type="button" ng-click="userloginform.reset()" class="btn btn-primary">Reset</button>
							</div>
						</div>
					</form>
				</div>
					<div class="col-md-1"></div>
			</div>
		</div>
    </div>

<?php include_once('template/footer.php'); ?>