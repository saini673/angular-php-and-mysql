	<?php
	  $title 				= 'User Register';
	  $angular_app 			= 'angular-validator-demo';
	  $angular_controller 	= 'DemoCtrl';
	  include_once('template/header.php');
	?>
    <div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="col-sm-3"></div>
				<div class="col-sm-6" ng-show="sucess_msg">
					<div class = "alert alert-success alert-dismissable" ng-show="sucess_msg">
						<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">&times;</button>
						<strong>Success!</strong> <span class="text-center" ng-repeat="message in msgs">{{ message }} </span>
					</div>
				</div>
				<div class="col-sm-3"></div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<form angular-validator-submit="submitMyForm()" name="myForm" class="form-horizontal" novalidate angular-validator>
						<h4 class="text-center">Register User Form</h4>
						<div class="form-group">
							<label class="col-sm-4 control-label">First Name</label>
							<div class="col-sm-8">
								<input  type = "text"
									name = "first_name"
									validate-on="first_name"
									class = "form-control"
									ng-model = "form.first_name"
									ng-pattern="/[a-zA-Z^ ]/"
									invalid-message="'Please enter character only.'"
									required-message="'Please enter first name.'"
									ng-minlength="3"
									invalid-message="'First name minimum 3 character required.'"
									ng-maxlength="15"
									invalid-message="'First name maximum 15 character required.'"
									required />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Last Name</label>
							<div class="col-sm-8">
								<input  type = "text"
									name = "last_name"
									validate-on="last_name"
									class = "form-control"
									ng-model = "form.last_name"
									ng-pattern="/[a-zA-Z^ ]/"
									invalid-message="'Please enter character only.'"
									required-message="'Please enter Last name.'"
									ng-minlength="3"
									invalid-message="'Last name minimum 3 character required.'"
									ng-maxlength="15"
									invalid-message="'Last name maximum 15 character required.'"
									required />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Email</label>
							<div class="col-sm-8">
								<input  type = "email"
									name = "email"
									validate-on="email"
									class = "form-control"
									id  = "email_id"
									ng-model = "form.email"
									invalid-message="'Enter valid email address'"
									required-message="'Please enter email address.'"
									required />
							</div>
							<span class="email_error" ng-repeat="error in errors">{{ error}}</span>
						</div>
						<hr>
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-4 control-label">City</label>
							<div class="col-sm-8">
								<input  type = "text"
										name = "city"
										class = "form-control"
										ng-model = "form.city"
										ng-pattern="/[a-zA-Z^ ]/"
										invalid-message="'Please enter character only.'"
										required-message="'Please enter city.'"
										validate-on="city"
										required />
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-4 control-label">Country</label>
							<div class="col-sm-8">
								<input  type = "text"
										name = "country"
										class = "form-control"
										ng-model = "form.country"
										ng-pattern="/[a-zA-Z^ ]/"
										invalid-message="'Please enter character only.'"
										required-message="'Please enter country.'"
										validate-on="country"
										required />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-4 control-label">Address</label>
							<div class="col-sm-8">
								<textarea name="address"
									class="form-control"
									validate-on="address"
									ng-model = "form.address"
									required-message="'Please enter address.'"
									required
								>
								</textarea>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-4 control-label">Phone Number</label>
							<div class="col-sm-8">
								<input  type = "text"
										name = "phone"
										class = "form-control"
										ng-model = "form.phone"
										ng-pattern="/^(\(?([0-9]{3})\)?)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/"
										invalid-message="'Please enter valid phone number.'"
										required-message="'Please enter phone number.'"
										validate-on="phone"
										required /> <br/><code>e.g (123-345-6859)</code>
							</div>
						</div>
						
						<hr>
						<div class="form-group">
							<label class="col-sm-4 control-label">Password</label>
							<div class="col-sm-8">
								<input  type = "password"
										name = "password"
										class = "form-control"
										ng-model = "form.password"
										validator = "passwordValidator(form.password) === true"
										invalid-message = "passwordValidator(form.password)"
										validate-on="dirty"
										required />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Confirm Password</label>
							<div class="col-sm-8">
								<input  type = "password"
										name = "confirmPassword"
										class = "form-control"
										ng-model = "form.confirmPassword"
										validator = "form.password === form.confirmPassword"
										validate-on="dirty"
										invalid-message = "'Passwords do not match!'"
										required />
							</div>
						</div>
						<hr>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<button type="submit" class="btn btn-primary">Submit</button>
								<button type="button" ng-click="myForm.reset()" class="btn btn-primary">Reset</button>
							</div>
						</div>
					</form>
				</div>
					<div class="col-md-2"></div>
			</div>
		</div>
    </div>

<!-- End container --->
   <?php include_once('template/footer.php'); ?>