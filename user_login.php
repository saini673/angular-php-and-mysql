<?php
	ob_start();
	require_once('functions/function.php');
	global $dbh;
	$post_data 		= json_decode(file_get_contents("php://input"));
	$column_field 	= array();
	$value_field 	= array();
	$email_field 	= array();
	
	
	$email  		=		$post_data->email;
	$password  		=		$post_data->password;
	$userAuth  		=		UserLogin('users',$email,$password);
	if($userAuth == 0){
		$message = array('login_message' => "", 'error' => 'Email and Password Invalid.');
		$reponse = json_encode($message);
		print_r($reponse);
	}else{
		session_start();
		$result				=	getUserDetail('users',$email,$password);
		$_SESSION['UserID'] = 	$result[0]->uid;
		$message = array('login_message' => "1", 'error' => '');
		$reponse = json_encode($message);
		print_r($reponse);
	}
?>

