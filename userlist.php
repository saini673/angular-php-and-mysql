<?php
	require_once('functions/function.php');
	header("Access-Control-Allow-Origin: *");
	header("Content-Type: application/json; charset=UTF-8");
	
	if(isset($_REQUEST['uid']) && !empty($_REQUEST['uid']))
	{
		$uid				=	$_REQUEST['uid'];
		$UserRecordById 	= 	getuserByUid('users',$uid);
		echo json_encode(array('error' => false,'userdetail' => $UserRecordById), JSON_HEX_TAG | JSON_HEX_APOS |JSON_HEX_QUOT |JSON_HEX_AMP );
	}
	else
	{
		$userDetail = getAllUser('users');
		echo json_encode(array('error' => false,'records' => $userDetail), JSON_HEX_TAG | JSON_HEX_APOS |JSON_HEX_QUOT |JSON_HEX_AMP );
	}
	
?>