<?php
	require_once('functions/function.php');
	global $dbh;
	$post_data 		= json_decode(file_get_contents("php://input"));
	
	$Image 					= $post_data->imagepath;
	$uid 					= $post_data->uid;
	$filename 				= $post_data->filename;
	$filename 				= explode('.',$filename);
	$filename 				= $filename[0];
	$ExistprofileImage		= existProfilePicById('user_profile',$uid);
	
	if($ExistprofileImage == 'exist'){
		$picDetail 		=	ProfilePicById('user_profile',$uid);
		unlink($picDetail->path);
		$UploadedFile	=   MoveCorpedImage($Image,$uid,$filename);
		$whereArr 		= 	array(); 
		$set_value 		= 	array(); 
		$post_value		=	array('image_name'=>$uid.'_'.$post_data->filename,'path'=>$UploadedFile,'pid'=>$picDetail->pid);
		 foreach($post_value as $key=>$data) {
			if($key == 'pid'){
				$whereArr[] = "$key = '$data'"; 
			}
			if($key != 'pid'){
				$set_value[] = "$key = '$data'";
			}			
		}
        $result 		= 	update('user_profile', $set_value, $whereArr);
		
		if ($result) {
			
			$message = array('message' => $UploadedFile,'error' => '');
			$reponse = json_encode($message);
			print_r($reponse);
		} 
		else {
			
			$message = array('message' => "", 'error' => 'Error In delete record');
			$reponse = json_encode($message);
			print_r($reponse);
		}
			
	}
	else if($ExistprofileImage == 'pass'){
		
		$UploadedFile			= MoveCorpedImage($Image,$uid,$filename);
		
		if(!empty($UploadedFile)){
			
			$column_field		=	array('id','image_name','path');
			$value_field		=	array($uid,$uid.'_'.$post_data->filename,$UploadedFile);
			$result 			= 	insert('user_profile', $column_field, $value_field);
			
			if ($result) {
				
				$message = array('message' => $UploadedFile, 'error' => '');
				$reponse = json_encode($message);
				print_r($reponse);
			} 
			else {
				
				$message = array('message' => "", 'error' => 'Error In delete record');
				$reponse = json_encode($message);
				print_r($reponse);
			}
		}
	}
	
?>

