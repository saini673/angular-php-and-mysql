-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 15, 2016 at 01:37 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `devproject`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `uid` int(11) NOT NULL,
  `first_name` varchar(225) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(225) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `address` text,
  `phone` varchar(100) DEFAULT NULL,
  `password` varchar(225) DEFAULT NULL,
  `block_user` int(11) NOT NULL DEFAULT '0',
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`uid`, `first_name`, `last_name`, `email`, `city`, `country`, `address`, `phone`, `password`, `block_user`, `create_date`, `update_date`) VALUES
(1, 'vishal', 'Saini', 'vishal.3ginfo@gmail.com', 'Jalandhar', 'india', '#235 R.S.D Colony Shahpur Kandi Jalandhar', '9803658980', '5e6a2abb1699a9c8aab680d8b251dc66', 0, '2016-03-01 13:14:08', '2016-03-14 10:56:52'),
(2, 'Rajwinder', 'Thakur', 'rajwinder@gmail.com', 'Chandigarh', 'India', 'Jugial Colony', '123-898-0988', '361633153a464830a1fe85dec5efab17', 0, '2016-03-01 13:14:08', '2016-03-03 11:57:15'),
(3, 'raju', 'kumar', 'rajukumar@gmail.com', 'Mohali', 'India', 'Mohali', 'fefef', 'c0ffa0667bf68af71b18a878f2e6c7dd', 0, '2016-03-01 13:14:08', '2016-03-10 12:40:24'),
(4, 'vishal', 'Thakur', 'rajeev386@yahoo.co.in', 'Pathankot', 'india', 'Pathankot -jugial highway', '123-345-4454', '29cc98a3c0cb9321fa574c21be5d76bf', 0, '2016-03-01 13:14:08', '2016-03-11 17:52:17'),
(9, 'harinder', 'Pal', 'harinderpal@yahoo.com', 'Chandigarh', 'india', 'Chd', '123-334-3432', '5e6a2abb1699a9c8aab680d8b251dc66', 0, '2016-03-09 10:35:59', '2016-03-14 10:57:56'),
(10, 'Joyti', 'Devi', 'joyti.devi@gmail.com', 'Chandigarh', 'india', '#455 33-B', '123-334-3432', '31112fb60a8913ef9283816647d286dd', 0, '2016-03-09 10:41:07', '2016-03-14 17:07:32'),
(12, 'arun', 'Pal', 'arunpal@gmail.com', 'Mohali', 'India', '3b2 Mohali', '876-987-9874', '408ac2f36ce4d4dcff807bd6a79054a9', 0, '2016-03-09 10:55:33', '2016-03-11 18:11:48');

-- --------------------------------------------------------

--
-- Table structure for table `user_profile`
--

CREATE TABLE `user_profile` (
  `pid` int(11) NOT NULL,
  `id` int(11) NOT NULL COMMENT 'User Id',
  `image_name` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_profile`
--

INSERT INTO `user_profile` (`pid`, `id`, `image_name`, `path`, `create_date`, `updated_date`) VALUES
(1, 1, '1_Lighthouse.jpg', 'uploads/1_Lighthouse.jpg', '2016-03-15 10:29:04', '2016-03-15 17:23:02');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`uid`);

--
-- Indexes for table `user_profile`
--
ALTER TABLE `user_profile`
  ADD PRIMARY KEY (`pid`),
  ADD KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `user_profile`
--
ALTER TABLE `user_profile`
  MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `user_profile`
--
ALTER TABLE `user_profile`
  ADD CONSTRAINT `user_profile_ibfk_1` FOREIGN KEY (`id`) REFERENCES `users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
